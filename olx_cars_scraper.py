# -*- coding: utf-8 -*-
import re
import datetime
import scrapy


name_map = {
    'an de fabricatie': 'year',
    'marca': 'brand',
    'model': 'model',
    'culoare': 'color',
    'cutie de viteze': 'gearbox',
    'capacitate motor': 'engine_capacity',
    'rulaj': 'mileage',
    'combustibil': 'fuel',
    'stare': 'state',
    'oferit de': 'offered_by',
    'caroserie': 'body'
}

months = {
    'ianuarie': 1,
    'februarie': 2,
    'martie': 3,
    'aprilie': 4,
    'mai': 5,
    'iunie': 6,
    'iulie': 7,
    'august': 8,
    'septembrie': 9,
    'octombrie': 10,
    'noiembrie': 11,
    'decembrie': 12
}


class OlxScraper(scrapy.Spider):
    name = 'olx'
    start_urls = [
        'https://www.olx.ro/auto-masini-moto-ambarcatiuni/autoturisme/',
    ]

    allowed_domains = ["olx.ro"]

    custom_settings = {
        "DOWNLOAD_DELAY": 0.2,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 20
    }

    def nows(self, text):
        return text.replace('\t', '').replace('\n', '').strip()

    def css_path_text(self, response, css_path, all_text=False):
        extract = response.css(f'{css_path}::text').extract()
        if not all_text:
            return self.nows(extract[0])
        else:
            return self.nows('\n'.join(extract))

    def get_date(self, text_date):
        match = re.search('La (?P<hour>\d{2}):(?P<minute>\d{2}), (?P<day>\d{2}) (?P<month>\S+) (?P<year>\d+)', text_date)
        date = datetime.datetime(
            int(match.group('year')),
            months[match.group('month')],
            int(match.group('day')),
            int(match.group('hour')),
            int(match.group('minute')),
        )
        return date.isoformat()

    def parse(self, response):

        for href in response.css('.marginright5.detailsLink::attr(href)'):
            url = href.extract()
            if 'www.autovit.ro' not in url:
                yield scrapy.Request(url, callback=self.parse_vehicle)

        page = response.css('.link.pageNextPrev span::text').extract()[-1]
        print(page)
        try:
            next_page = response.css('.pageNextPrev::attr(href)').extract()[-1]
            yield scrapy.Request(next_page, callback=self.parse)
        except IndexError:
            print('End')

    def parse_vehicle(self, response):
        print(response.url)

        data = {}
        for table in response.css('table.item'):
            name = self.css_path_text(table, 'th')
            try:
                value = self.css_path_text(table, '.value strong a')
            except IndexError:
                value = self.css_path_text(table, '.value strong')
            field = name_map.get(name.lower(), name.lower())
            data[field] = value 

        data['id'] = response.url
        data['title'] = self.css_path_text(response, '.offer-titlebox h1')
        data['description'] = self.css_path_text(response, '#textContent')
        data['price'] = self.css_path_text(
            response,
            '.price-label strong.xxxx-large'
        )

        data['location'] = self.css_path_text(
            response,
            '.show-map-link strong'
        )

        text_date = self.css_path_text(
            response,
            '.offer-titlebox__details em', 2
        )

        data['date'] = self.get_date(text_date)

        yield data
