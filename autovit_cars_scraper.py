# -*- coding: utf-8 -*-
import re
import datetime
import scrapy


name_map = {
    'oferit de': 'offered_by',
    'categorie': 'category',
    'marca': 'brand',
    'model': 'model',
    'anul fabricatiei': 'year',
    'km': 'mileage',
    'vin': 'vin',
    'capacitate cilidrica': 'engine_capacity',
    'putere': 'power',
    'transmisie': 'transmission',
    'combustibil': 'fuel',
    'culoare': 'color',
    'cutie de viteze': 'gearbox',
    'norma de poluare': 'euro_category',
    'emisii co2': 'carbon_emissions',
    'stare': 'state',
    'caroserie': 'body',
    'filtru de particule': 'particle_filter',
    'numar de portiere': 'door_number',
    'tara de origine': 'country',
    'inmatriculat': 'registered',
    'primul proprietar': 'first_owner',
    'carte de service': 'service_book',
}


months = {
    'ianuarie': 1,
    'februarie': 2,
    'martie': 3,
    'aprilie': 4,
    'mai': 5,
    'iunie': 6,
    'iulie': 7,
    'august': 8,
    'septembrie': 9,
    'octombrie': 10,
    'noiembrie': 11,
    'decembrie': 12
}


class AutovitScraper(scrapy.Spider):
    name = 'autovit'
    start_urls = [
        'https://www.autovit.ro/autoturisme/',
    ]

    allowed_domains = ["autovit.ro"]

    custom_settings = {
        "DOWNLOAD_DELAY": 0.2,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 20
    }

    def nows(self, text):
        return text.replace('\t', '').replace('\n', '').strip()

    def css_path_text(self, response, css_path, all_text=False):
        extract = response.css(f'{css_path}::text').extract()
        if not all_text:
            return self.nows(extract[0])
        else:
            return self.nows('\n'.join(extract))

    def get_date(self, text_date):
        match = re.search('(?P<hour>\d{2}):(?P<minute>\d{2}), (?P<day>\d{2}) (?P<month>\S+) (?P<year>\d+)', text_date)
        date = datetime.datetime(
            int(match.group('year')),
            months[match.group('month')],
            int(match.group('day')),
            int(match.group('hour')),
            int(match.group('minute')),
        )
        return date.isoformat()

    def parse(self, response):

        for href in response.css('.offers.list article::attr(data-href)'):
            url = href.extract()
            yield scrapy.Request(url, callback=self.parse_vehicle)

        try:
            next_page = response.css('.next.abs a::attr(href)').extract()[0]
            yield scrapy.Request(next_page, callback=self.parse)
        except IndexError:
            print('End')

    def parse_vehicle(self, response):
        print(response.url)

        data = {}
        for table in response.css('.offer-params__list li.offer-params__item'):
            name = self.css_path_text(table, '.offer-params__label')
            try:
                value = self.css_path_text(table, '.offer-params__value a.offer-params__link')
            except IndexError:
                value = self.css_path_text(table, '.offer-params__value')
            field = name_map.get(name.lower(), name.lower())
            data[field] = value

        data['id'] = response.url
        data['title'] = self.css_path_text(
            response, 'h1.offer-title.big-text', all_text=True
        )

        data['description'] = self.css_path_text(response, '.offer-description div')
        price = self.css_path_text(response, '.offer-price__number')
        currency = self.css_path_text(response, '.offer-price__currency')
        data['price'] = price
        data['currency'] = currency

        data['location'] = self.css_path_text(
            response,
            '.seller-box__seller-address__label'
        )

        text_date = self.css_path_text(
            response,
            '.offer-meta__value'
        )

        data['date'] = self.get_date(text_date)

        yield data